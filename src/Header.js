import React from "react"
import LocalWeather from "./localWeather"
import CityWeather from "./cityWeather"
import logo from "./images/logo.svg"

import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";

function Header () {
    return (

            <Router>
            <div>
            <header>
                <img src={logo} alt="weather icon" />
                <p> Weather APP</p>

                <button>
                    <Link to="/localweather">localweather</Link>
                </button>
                <button>
                    <Link to="/cityweather">cityweather</Link>
                </button>
                </header> 

                {/* A <Switch> looks through its children <Route>s and
                    renders the first one that matches the current URL. */}
                <Switch>
                <Route exact path="/localweather">
                    <LocalWeather />
                </Route>
                <Route path="/cityweather">
                    <CityWeather />
                </Route>
                </Switch>
            </div>
            </Router>

    )
}

export default Header
