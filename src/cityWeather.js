import React, { Component } from "react"
import config from "./config.json"
import WeatherDisplay from "./weatherDisplay"

class cityWeather extends Component {
    constructor () {
        super ()
        this.state = {
            cityName: "warsaw",
            weatherFor: "null",
            data: ""
        }
        this.getWeather = this.getWeather.bind(this)
        this.handleChange = this.handleChange.bind(this)
    }

    getWeather () {
        this.setState({
            weatherFor: this.state.cityName
        })
        fetch(`https://api.openweathermap.org/data/2.5/forecast?q=${this.state.cityName}&appid=${config.KEY}`)
        .then(response => response.json())
        .then(data => 
           this.setState({
              data: data
        }))
        .catch(error => alert(error))
    }

    showError(error) {
        switch(error.code) {
          case error.PERMISSION_DENIED:
            alert("User denied the request for Geolocation.")
            break;
          case error.POSITION_UNAVAILABLE:
            alert("Location information is unavailable.")
            break;
          case error.TIMEOUT:
            alert("The request to get user location timed out.")
            break;
          case error.UNKNOWN_ERROR:
            alert("An unknown error occurred.")
            break;
        default:
            alert("An unknown error occurred.")
        }
      }

    handleChange (event) {
        //const {name, value} = event.target
        this.setState({
            cityName: event.target.value 
        })
    }

    render () {
        return (
            <div className="cityWeather">
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <label>
                    <input 
                        className="cityWeather_input"
                        type="text"
                        value={this.state.cityName}
                        placeholder="Enter city name"
                        onChange={this.handleChange}
                    />
                    <button className="cityWeather_button" onClick={this.getWeather}>Get Weather!</button>
                </label>
                { this.state.data ? <WeatherDisplay data={this.state}/> : null }
            </div>
        )
    }
}

export default cityWeather