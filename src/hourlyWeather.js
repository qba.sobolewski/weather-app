import React from "react"
import icon_rain from "./images/wi-day-rain.svg"
import icon_cloudy from "./images/wi-cloudy.svg"
import icon_sunny from "./images/wi-day-sunny.svg"

class hourlyWeather extends React.Component {

    icon (param) {
        if (param === "Rain") {
            return icon_rain
        } else if (param === "Clouds") {
            return icon_cloudy
        } else {
            return icon_sunny
        }
    }

    style (param) {
        if (param === "Rain") {
            return {backgroundColor: "#696969"}
        } else if (param === "Clouds") {
            return {backgroundColor: "#bfbfbf"}
        } else if (param === "Clear") {
            return {backgroundColor: "#6199f2"}
        } else {
            return {backgroundColor: "dbda86"}
        }
    }

    render () {
        const elements = []

        for (let i = 0; i < this.props.data.data.data.list.length; i++){

            elements.push(
            <div style={this.style(this.props.data.data.data.list[i].weather[0].main)}>
                <p>time: {JSON.stringify(this.props.data.data.data.list[i].dt_txt)}</p>
                <p>{this.props.data.data.data.list[i].weather[0].main}</p>
                <img src={this.icon(this.props.data.data.data.list[i].weather[0].main)} alt="weather icon" height= "100" />
                <p>temperature: {JSON.stringify(Math.floor(this.props.data.data.data.list[i].main.temp - 273))} C</p>
                <p>fells like: {JSON.stringify(Math.floor(this.props.data.data.data.list[i].main.feels_like -273))} C</p>
                <p>pressure: {JSON.stringify(this.props.data.data.data.list[i].main.pressure)}hPa</p>
                <p>humidity: {JSON.stringify(this.props.data.data.data.list[i].main.humidity)}%</p>               
            </div>
            )
        }

        return (
            <div className="hourlyWeatherElement">
                {elements}
            </div>
        )
    }
}

export default hourlyWeather