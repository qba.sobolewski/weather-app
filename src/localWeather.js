import React, {Component} from "react"
import config from "./config.json"
import WeatherDisplay from "./weatherDisplay"

class localWeather extends Component {
    constructor() {
        super()
        this.state = {
            isLoading: false,
            latitude: "",
            longitude: "",
            data: ""
        }
        this.getLocation = this.getLocation.bind(this)
        this.getCoordinates = this.getCoordinates.bind(this)
        this.getWeather = this.getWeather.bind(this)
    }

    getLocation() {
      this.setState({
        isLoading: true
      })
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(this.getCoordinates, this.showError);
        } else {
          alert("Geolocation is not supported by this browser.");
        }
      }

      getCoordinates(position) {
          console.log(position)
          this.setState({
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
              isLoading: false
          })
          this.getWeather()
      }

      getWeather () {
          fetch(`https://api.openweathermap.org/data/2.5/forecast?lat=${this.state.latitude}&lon=${this.state.longitude}&appid=${config.KEY}`)
          .then(response => response.json())
          .then(data => 
             this.setState({
                data: data
          }))
          .catch(error => alert(error))
      }

      showError(error) {
        switch(error.code) {
          case error.PERMISSION_DENIED:
            alert("User denied the request for Geolocation.")
            break;
          case error.POSITION_UNAVAILABLE:
            alert("Location information is unavailable.")
            break;
          case error.TIMEOUT:
            alert("The request to get user location timed out.")
            break;
          case error.UNKNOWN_ERROR:
            alert("An unknown error occurred.")
            break;
        default:
            alert("An unknown error occurred.")
        }
      }

    render () {
        return (
            <div className="localWeather">
              <br/>
              <br/>
              <br/>
              <br/>
              <br/>
              <br/>
                <button onClick={this.getLocation}>Get your local weather</button>
                {this.state.isLoading ? <p>Loading...</p> : null}
                { this.state.data ? <WeatherDisplay data={this.state}/> : null }
            </div>
        )
    }
}

export default localWeather