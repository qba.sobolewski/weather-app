import React from "react"
import HourlyWeather from "./hourlyWeather"

function weatherDisplay (props) {

    return (
        <div>
            <h4>Weather for: {props.data.weatherFor ? props.data.weatherFor : "Local weather"}</h4>
            {
            props.data.weatherFor 
            ? 
            null 
            : 
            <div> 
                <p>latitude {props.data.latitude}</p> 
                <p>longitude {props.data.longitude}</p>
            </div>
            }
            <br />
            <p>Temeperature:{JSON.stringify(props.data.data.list[0].main.temp)}</p>
            <HourlyWeather data = {props}/>
        </div>
    )
}

export default weatherDisplay